<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de usuario</h1>
      <form method="post" action="/user/update">
        <input type="hidden"  name="id" value="<?php echo $user->id ?>">

        <div class="form-group">
          <label>Nombre:</label>
          <input type="text" class="form-control" name="name" value="<?php echo $user->name ?>">
        </div>

        <div class="form-group">
          <label>Apellidos:</label>
          <input type="text" class="form-control" name="surname" value="<?php echo $user->surname ?>">
        </div>
        <div class="form-group">
          <label>Edad:</label>
          <input type="text" class="form-control" name="age" value="<?php echo $user->age ?>" >
        </div>
        <div class="form-group">
          <label>Email address:</label>
          <input type="text" class="form-control" name="email" value="<?php echo $user->email ?>">
        </div>

        <input type="submit" class="btn btn-default"><br>

      </form>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>


