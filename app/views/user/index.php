<!doctype html>
<html lang="en">
<head>
 <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">


    <h1>Lista de usuarios </h1>

    <table class="table table-striped">
      <thead>
        <tr>
          <th> Id </th>
          <th> Nombre </th>
          <th> Apellidos </th>
          <th> Edad </th>
          <th> Email </th>
        <th> Acciones </th>

        </tr>
      </thead>
      <tbody>
        <?php foreach ($users as $user): ?>
          <tr>
            <td><?php echo $user->id ?></td>
            <td><?php echo $user->name ?></td>
            <td><?php echo $user->surname ?></td>
            <td><?php echo $user->age ?></td>
            <td><?php echo $user->email ?></td>
             <td><a href="/user/show/<?php echo $user->id ?>">Ver</a></td>
          </tr>
        <?php endforeach ?>
      </tbody>

</main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
