<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Alta de usuario</h1>
      <form method="post" action="/user/store">

        <div class="form-group">
          <label>Nombre:</label>
          <input type="text" class="form-control" name="name">
        </div>

        <div class="form-group">
          <label>Apellidos:</label>
          <input type="text" class="form-control" name="surname">
        </div>
        <div class="form-group">
          <label>Edad:</label>
          <input type="text" class="form-control" name="age">
        </div>
        <div class="form-group">
          <label>Email address:</label>
          <input type="text" class="form-control" name="email">
        </div>

        <input type="submit" class="btn btn-default"><br>

      </form>
    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>


</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>


