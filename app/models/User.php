<?php
namespace App\models;

use PDO;
use Core\Model;

require_once'../core/Model.php';

class User extends Model{

    function __construct(){ }

    public static function all(){
        $db=User::db();
        $statement = $db->query('SELECT * FROM users');
        $users=$statement->fetchAll(PDO::FETCH_CLASS, User::class);
        return $users;
    }//fin all

    public static function find($id){
        //var_dump($id);
        //exit();
        $db=User::db();
        $stmt = $db->prepare('SELECT * FROM table WHERE id=:id ');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, User::class);
        $user = $stmt->fetch(PDO::FETCH_CLASS);


        return $user;
    }



}//fin User




?>
