<?php

namespace App\Controllers;

use \app\models\User;
require_once '../app/models/User.php';


class UserController{

  function __construct()
  {
        // echo "En UserController ";
  }

  public function index(){
       $users= User::all(); //acceder a metodos estaticos sin obj

       // echo "<pre>";
       // var_dump($users);
       // exit();
       require "../app/views/user/index.php";
     }

     public function show($arguments){
       $id=(int) $arguments[0];
       $user=User::find($id);
       require "../app/views/user/show.php";
     }
   }


   ?>
