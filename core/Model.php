<?php

namespace Core;
use PDO;

class Model {

    function __construct(){

    }
    protected static function db(){
        $dsn = 'mysql:dbname=mvc18;host=127.0.0.1';
        $usuario = 'root';
        $contraseña = 'root';
        try {
            $db = new PDO($dsn, $usuario, $contraseña);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
        return $db;
    }
}
