<?php


class App
{

    function __construct()
    {
        //echo "Clase App<br>";


        if (isset($_GET['url'])) {
            $url = $_GET['url'];//$_GET es valido
        }else{
            $url ='home';
        }


        //vamos a usar la url de la siguiente manera: controller/method/arguments

        $arguments = explode('/', trim($url, '/'));
        $controllerName=array_shift($arguments);
        $controllerName=ucwords($controllerName) . "Controller";
        if (count($arguments)) {
            $method= array_shift($arguments);
        }else{
            $method="index";
        }


        $file="../app/controllers/$controllerName" . ".php";

        if (file_exists($file)){
            require_once $file;
        }else{
            header("HTTP/1.0 404 Not Found");
            echo "No encontrado";
            die();
        }
        require_once $file;

        $controllerName = '\\App\\Controllers\\' . $controllerName;
        $controllerObject = new $controllerName;
        if (method_exists($controllerName, $method)) {
            $controllerObject->$method($arguments);
        }else{
            header("HTTP/1.0 404 Not Found");
            //echo "PHP continues.\n";
            die();
        }


    }//constructor

}//clase App

?>
